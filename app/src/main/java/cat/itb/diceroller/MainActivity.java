package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button resetButton;
    private Button rollButton;
    private ImageView dado1;
    private ImageView dado2;
    int[] imagenesDados = {R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3, R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resetButton = findViewById(R.id.reset_button);
        rollButton = findViewById(R.id.roll_button);
        dado1 = findViewById(R.id.dado1);
        dado2 = findViewById(R.id.dado2);

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dado1.setVisibility(View.INVISIBLE);
                dado2.setVisibility(View.INVISIBLE);
            }
        });

        dado1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dado1.setImageResource(imagenesDados[(int) (Math.floor(Math.random() * 6))]);
            }
        });

        dado2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dado2.setImageResource(imagenesDados[(int) (Math.floor(Math.random() * 6))]);
            }
        });

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dado1.setVisibility(View.VISIBLE);
                dado2.setVisibility(View.VISIBLE);
                rollButton.setVisibility(View.VISIBLE);
                rollButton.setText("Dice Rolled");
                final Random diceNumber = new Random();
                int roll = diceNumber.nextInt(6)+1;
                int roll2 = diceNumber.nextInt(6)+1;
                if (roll2+roll==12){
                    Toast toast = Toast.makeText(MainActivity.this, "JACKPOT!", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP,0,250);
                    toast.show();
                }
                switch (roll){
                    case 1:
                        dado1.setImageResource(R.drawable.dice_1);
                        break;
                    case 2:
                        dado1.setImageResource(R.drawable.dice_2);
                        break;
                    case 3:
                        dado1.setImageResource(R.drawable.dice_3);
                        break;
                    case 4:
                        dado1.setImageResource(R.drawable.dice_4);
                        break;
                    case 5:
                        dado1.setImageResource(R.drawable.dice_5);
                        break;
                    case 6:
                        dado1.setImageResource(R.drawable.dice_6);
                        break;
                }

                switch (roll2){
                    case 1:
                        dado2.setImageResource(R.drawable.dice_1);
                        break;
                    case 2:
                        dado2.setImageResource(R.drawable.dice_2);
                        break;
                    case 3:
                        dado2.setImageResource(R.drawable.dice_3);
                        break;
                    case 4:
                        dado2.setImageResource(R.drawable.dice_4);
                        break;
                    case 5:
                        dado2.setImageResource(R.drawable.dice_5);
                        break;
                    case 6:
                        dado2.setImageResource(R.drawable.dice_6);
                        break;
                }

            }
        });
    }
}